from setuptools import setup, find_packages

def readme():
    with open("README.md") as f:
        return f.read()

setup(
    name="comutils",
    version="0.1.0",
    license="MIT",
    long_description=readme(),
    author="Nicola Mosco",
    author_email="nicola.mosco@gmail.com",
    packages=find_packages(),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: MIT",
        "Programming Language :: Python :: 3.9"
    ],
    install_requires=[
        "docopt",
        "toml",
        "suds-py3",
        "requests",
        "cx-oracle",
        "lxml",
        "pandas",
        "numpy",
        "paramiko",
        "pyyaml",
        "termcolor",
        "colorama",
        "GitPython"
    ],
    entry_points={
        "console_scripts": [
            "fom_plan=comutils.scripts.misc.fom_plan:main",
            "dbquery=comutils.scripts.misc.dbquery:main",
            "gtea=comutils.scripts.gitea_api.gtea:main",
            "allinea_auto=comutils.scripts.ops.allinea_auto:main"
        ]
    },
    scripts=["comutils/scripts/ops/get_pr"]
)
