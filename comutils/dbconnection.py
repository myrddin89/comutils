from os import PathLike
from typing import Dict, Union
import cx_Oracle as cxo
import pandas as pd
from tabulate import tabulate

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)

from .serverconfig import ServerConfig, serverconfig


# class QueryBuilder(object):
#     def __init__(self, parts) -> None:
#         super().__init__()
#         for k, v in parts.items():
#             setattr(self, k, v)

#     def __call__(self, order_id: str) -> str:
#         if not order_id:
#             raise RuntimeError("ERROR: please provide order id")
#         table = getattr(self, "table", "COM_OWN.COM_ORDER_ITEM")
#         col_names = ",".join(self.columns)
#         col_names = col_names if col_names else "*"
#         cond_str = " and ".join(self.conditions)
#         ordering = getattr(self, "ordering", None)
#         joins = getattr(self, "joins", None)
#         if ordering:
#             ord_str = " order by " + ",".join(ordering)
#         else:
#             ord_str = None
#         if joins:
#             join_str = ""
#             for join in joins:
#                 join_cond_str = ""
#                 for join_cond in join['conditions']:
#                     join_cond_str += join_cond
#                 join_str = f" {join['type']} {join['table']} ON {join_cond_str}"
#         else:
#             join_str = None
#         query = f"select {col_names} from {table}"
#         if join_str:
#             query += join_str
#         if cond_str:
#             query += f" where COM_ORDER_REF_ID='{order_id}' and {cond_str}"
#         else:
#             query += f" where COM_ORDER_REF_ID='{order_id}'"
#         if ord_str:
#             query += ord_str
#         return query


class QueryBuilder(object):
    def __init__(self, sql_query: str) -> None:
        super().__init__()
        self.sql_query = sql_query

    def __call__(self, **kwargs) -> str:
        order_id = kwargs.get("order_id", None)
        if not order_id:
            raise RuntimeError("ERROR: please provide order id")
        return self.sql_query.format(**kwargs)


def connect(config: ServerConfig) -> cxo.Connection:
    host = config.local_hostname
    port = config.local_port
    service = config.service
    dsn = cxo.makedsn(host, port, service_name=service)
    user = config.username
    pwd = config.password
    return cxo.connect(dsn=dsn, user=user, password=pwd, encoding="UTF-8")


def read_sql(stmt: str, connection: cxo.Connection, limit: Union[int,None]=None) -> pd.DataFrame:
    if limit:
        df = next(pd.read_sql(stmt, con=connection, chunksize=limit))
    else:
        df = pd.read_sql(stmt, con=connection)
    return tabulate(df, showindex=False, headers=df.columns)


def build_query(sql_query: str, **kwargs) -> str:
    builder = QueryBuilder(sql_query)
    return builder(**kwargs)
