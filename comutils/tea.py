import tibco.tea as tea
from .serverconfig import ServerConfig


_default_config = {
    "enable_class_generation": True
}


def connect_tea(config: ServerConfig, *args, **kwargs):
    local_hostname = config.local_hostname
    local_port = config.local_port
    url = f"http://{local_hostname}:{local_port}"
    conf = _default_config
    return tea.EnterpriseAdministrator(*args, url=url, config=conf, **kwargs)
