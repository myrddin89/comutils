#! /usr/bin/env python

"""gtea.py -- Git Tea API Python client

USAGE:
    gtea.py [options]
    gtea.py [options] <command> [<name>] [<params>...]

ARGUMENTS:
    <command>           Git Tea API command. See <https://try.gitea.io/api/swagger#>
                        for further deteails. Without any arguments the program produces
                        the list of available commands.
    <params>            Optional list of parameters to be passed when calling the API.
                        The parameters should be specified as key-value pairs: 'K=V'.
    <name>              Repository or Organization name, if required.

OPTIONS:
    -h, --help          Show this help message and exit.
    -V, --version       Show version info and exit.
    -f FILE             Specify configuration from FILE in TOML format. The configuration
                        file should have at least an 'authentication' section
                        with the token used for Bearer authentication;
                        and a 'repository' sectio with then owner name.
                        If not specified the configuration is read from '~/com/config/gitea_config.toml'
    -O OWNER            Overrides the owner of the repo.
    --resp-headers      Return response headers instead.
    -v                  Print debug information.
"""

import sys
from typing import Dict
from docopt import docopt
from pathlib import Path
import requests as api
import logging
from ...comlog import set_logging_level
from ...gitea import gitea_api as gapi
from ...config import Config, gitea_config, auth_info, repo_info

_version = "0.1.0"
_logger = logging.getLogger("gtea")


def _info(msg):
    _logger.info(msg)


def _error(msg):
    _logger.error(msg)


def _debug(msg):
    _logger.debug(msg)


def parse_params(args):
    params = args["<params>"]
    if not params:
        return {}
    params_dict = {}
    try:
        for p in params:
            k, v = p.split("=")
            params_dict[k] = v
    except Exception as e:
        _error(f"Cannot parse params: {e}")
        sys.exit(1)
    return params_dict


def api_command(args):
    return args["<command>"]


def parse_args(args: Dict) -> Config:
    gtea_conf = gitea_config(args)
    repo = repo_info(gtea_conf)
    owner = args["-O"] if args["-O"] else repo["owner"]
    config_dict = {
        "command": api_command(args),
        "url_params": parse_params(args),
        "auth": auth_info(gtea_conf),
        "repo": repo,
        "owner": owner,
        "base_url": repo["url"],
        "name": args["<name>"]
    }
    return Config(config_dict)


def main():
    args = docopt(__doc__, version=f"gtea.py version {_version}")
    set_logging_level(args)
    _debug(args)
    config = parse_args(args)
    try:
        response = gapi.gtea(config)
    except Exception as e:
        _error(f"Cannot resolve command to request: {e}")
        sys.exit(1)
    if args["--resp-headers"]:
        print(response.headers)
    else:
        print(response.text)


if __name__ == '__main__':
    main()
