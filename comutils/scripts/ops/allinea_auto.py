#! /usr/bin/env python

"""allinea_auto -- Allinea source branch with target branch
USAGE:
    allinea_auto [options] <source> <target>

ARGUMENTS:
    <source>                Source branch. For instance, lab/canvas_aprile_2022.
    <target>                Target branch. For instance, develop
                            canvas_aprile_2022.

OPTIONS:
    -h, --help              Print this help message and exit.
    -V, --version           Print version information and exit.
    -f FILE                 Specify configuration file.
    -O OWNER                Override owner as given in configuration.
    -v                      Print debug information."""

import sys
from typing import Dict
from pathlib import Path
from docopt import docopt
from datetime import datetime
import logging
import git
import json
from pprint import pprint
from ...comlog import set_logging_level
from ...config import Config, gitea_config, auth_info, repo_info
from ...gitea import gitea_api as gapi

_version = "0.1.0"
_logger = logging.getLogger("gtea")


def _info(msg):
    _logger.info(msg)


def _error(msg):
    _logger.error(msg)


def _debug(msg):
    _logger.debug(msg)


def get_branches(args):
    return args["<source>"], args["<target>"]


def make_config(gtea_conf, owner, repo, source, target):
    config_dict = {
        "command": "repos",
        "url_params": { "limit": 1000 },
        "auth": auth_info(gtea_conf),
        "repo": repo,
        "owner": owner,
        "base_url": repo["url"],
        "name": owner
    }
    return Config(config_dict)


def repos_lookup(config: Config):
    api_builder = gapi.request_mapping["repos"]
    request, fapi, payload, params = api_builder(
        name=config.name, owner=config.owner, **config.url_params)
    response = gapi.gitea_request(
        fapi, config.base_url, request, auth=config.auth, json=payload, params=params)
    repos = json.loads(response.text)
    return repos['err
    # for r in repos:
        # yield r["name"]

def main():
    args = docopt(__doc__, version=f"allinea_auto version {_version}")
    set_logging_level(args)
    source, target = get_branches(args)
    gtea_conf = gitea_config(args)
    repo = repo_info(gtea_conf)
    dir_allinea = Path(f"allineamento_{datetime.now().isoformat()}")
    Path.mkdir(dir_allinea)
    _info(f"Using working directory: {dir_allinea}")
    owner = args["-O"] if args["-O"] else repo["owner"]
    config = make_config(gtea_conf, owner, repo, source, target)
    repos = repos_lookup(config)
    # pprint(sorted(repos))
    print(repos)


if __name__ == '__main__':
    main()
