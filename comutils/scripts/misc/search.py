#! /usr/bin/env python

"""search.py -- Programmatic interface to elastic search

USAGE:
    search.py [-hV] <env>

This script reads JSON document from stdin and send request to elastic search.

ARGUMENTS:
    <env>                   Query Elasticsearch on <env>.

OPTIONS:
    -h, --help              Print this help message and exit.
    -V, --version           Print version information and exit."""


import sys
import elasticsearch as es
from docopt import docopt
from pathlib import Path
from comutils.comutils import sshforwarder as ssh


__version__ = 'v0.1.0'

_default_config_name = 'elastic_config.json'


def query_elastic(server):
    api_url = f"http://localhost:{server.local_port}"
    client = es.Elasticsearch(api_url)
    index = "fw.fom.lm.all"
    # print(client.info())



def main():
    args = docopt(__doc__, version=f"search.py version {__version__}")
    #payload = sys.stdin.read()
    env = args["<env>"]
    conf_path = Path.home() / f"com/config/{env}/{_default_config_name}"
    server = ssh.ServerConfig(conf_path)
    ssh.with_tunnel(query_elastic, server)

if __name__ == '__main__':
    main()
