#! /usr/bin/env python

"""inspect_service.py -- Inspect WSDL given on the command line

USAGE:
    inspect_service.py [-hx] <wsdl> [<node>]

ARGUMENTS:
    <wsdl>                  WSDL file defining the service.
    <node>                  A node to create and inspect.

OPTIONS:
    -h, --help              Print this help message and exit.
    -V, --version           Print version information and exit.
    -x                      Treat <wsdl> as a bare XML file."""

import sys
from docopt import docopt
from suds.client import Client
from pathlib import Path
import pydoc
from xml.dom import minidom

__version__ = "v0.1.0"


def pager(x):
    pydoc.pager(str(x))


def create_node(client, name=None):
    if name:
        return client.factory.create(name)
    else:
        return None


def main():
    args = docopt(__doc__, version="inspect_service.py version {__version__}")
    wsdl = Path(args["<wsdl>"])
    if args["-x"]:
        xml_doc = minidom.parse(wsdl.resolve().as_posix())
        print(xml_doc.toprettyxml())
        return
    node_name = args["<node>"]
    client = Client(wsdl.resolve().as_uri())
    try:
        node = create_node(client, node_name)
    except Exception as e:
        print(e, file=sys.stderr)
    else:
        pager(node if node else client)


if __name__ == '__main__':
    main()
