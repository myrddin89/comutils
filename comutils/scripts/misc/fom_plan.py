#! /usr/bin/env python

"""fom_plan.py -- Retrieve info about FOM plans.

USAGE:
    fom_plan.py [-hVpsm] [-w WSDL -e ENV] <orderid>

ARGUMENTS:
    <orderid>                   Order ID as in COM_ORDER_REF_ID.

OPTIONS:
    -h, --help                  Print this help message and exit.

    -V, --version               Print version information and exit.
    -w, --wsdl WSDL             Path to 'OrderServiceHttp.wsdl'.
    -e ENV                      Environment name to use if not given by <orderid> path.
    -p                          Interprets <orderid> as directory name.
    -s                          Sort plan items by numeric ID.
    -m                          Show milestone info."""

from docopt import docopt
from pathlib import Path
from suds.client import Client
from suds.wsse import Security, UsernameToken
import logging
import sys
import colorama
from termcolor import colored

colorama.init()

logging.basicConfig(format="$levelname: $message",
                    style="$", level=logging.INFO)
logging.getLogger("suds.client").setLevel(logging.INFO)
logger = logging.getLogger(__name__)

__version__ = "v0.1.0"
_default_wsdl_path = "~/com/FOM/OrderServiceHttp.wsdl"
_envs_dir = "~/com/FOM/envs"


def _error(msg):
    logger.error(colored(msg, 'red'))


def _info(msg):
    logger.info(msg)


def _debug(msg):
    logger.debug(msg)


def _warn(msg):
    logger.warning(msg)

# class ExternalBusinessPlugin(object):
#     def marshalled(self, context):
#         body = context.envelope.getChild("Body")
#         plan_req = body[0]
#         plan_req.set("ExternalBusinessTransactionID", "123")


def process_request(client: Client, orderid: str, location=None, password="admin") -> None:
    # security = Security()
    # token = UsernameToken("admin", "admin")
    # token.setnonce()
    # token.setcreated()
    # security.tokens.append(token)
    # client.set_options(wsse=security)
    request_plan = f"""
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ord="http://www.tibco.com/aff/orderservice">
   <soapenv:Header>
      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
         <wsse:UsernameToken>
            <wsse:Username>admin</wsse:Username>
            <wsse:Password>{password}</wsse:Password>
         </wsse:UsernameToken>
      </wsse:Security>
   </soapenv:Header>
   <soapenv:Body>
      <ord:GetOrderExecutionPlanRequest ExternalBusinessTransactionID="123">
         <ord:orderRef>{orderid}</ord:orderRef>
      </ord:GetOrderExecutionPlanRequest>
   </soapenv:Body>
</soapenv:Envelope>"""
    request_details = f"""
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ord="http://www.tibco.com/aff/orderservice">
   <soapenv:Header>
      <wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
         <wsse:UsernameToken>
            <wsse:Username>admin</wsse:Username>
            <wsse:Password>{password}</wsse:Password>
         </wsse:UsernameToken>
      </wsse:Security>
   </soapenv:Header>
    <soapenv:Body>
        <ord:GetOrderDetailsRequest ExternalBusinessTransactionID="123">
            <ord:orderRef>{orderid}</ord:orderRef>
        </ord:GetOrderDetailsRequest>
    </soapenv:Body>
</soapenv:Envelope>"""
    if location is not None:
        client.set_options(location=location)
    response_plan = client.service.GetOrderExecutionPlan(__inject={"msg": request_plan})
    response_details = client.service.GetOrderDetails(__inject={"msg": request_details})
    return response_plan, response_details


def colored_status(status):
    if status == 'EXECUTION':
        return colored(status, 'magenta')
    elif status == 'COMPLETE':
        return colored(status, 'green')
    elif status == 'PENDING':
        return colored(status, 'grey', attrs=["bold"])
    elif status == 'CANCELLED':
        return colored(status, 'yellow')
    elif status == 'ERROR':
        return colored(status, 'red')
    else:
        return status


def colored_id(item_id):
    return colored(item_id, 'yellow', attrs=['bold'])


def find_correlation_id(header):
    for udf in header.udf:
        if udf.name == 'CorrelationID':
            return udf.value


def pretty(orderid, response, sort_items=False, show_milestones=False):
    resp_plan, resp_details = response
    plan = resp_plan.plan
    corr_id = find_correlation_id(resp_details.header)
    count = 1
    print(f"Plan info for OrderRefID ({colored(orderid, 'magenta')}) - CorrelationID ({colored(corr_id, 'yellow')}) - PlanID ({colored(plan.planID, 'yellow')}) - OrderID ({colored(plan.orderID, 'yellow')}).")
    print(f"Plan status: {colored_status(plan.status)}")
    plan_items = plan.planItem
    if sort_items:
        plan_items = sorted(plan_items, key=lambda pi: int(pi.planItemID))
    print("{:<4s} {:^15s} {:^40s} {:^45s} {:^15s}".format('#', 'ID', 'NAME', 'FRAGMENT ID', 'STATUS'))
    for pi in plan_items:
        count_id = f"{count}."
        pli_id = f"({colored_id(pi.planItemID)})"
        pli_name = f"{pi.planItemName}"
        pli_uid = f"{colored(pi.planFragmentUniqueID, 'cyan')}"
        pli_status = f"{colored_status(pi.status)}"
        print(f"{count_id:4s} {pli_id:30.30s} | {pli_name:<40.40s} | {pli_uid:<50.50s} | {pli_status:<s}")
        if show_milestones:
            print("\tMilestones:")
            count_m = 1
            for m in pi.milestone:
                print(
                    f"\t{count_m}) {m.milestoneID} ({colored_status(m.status)})", end="")
                if hasattr(m, "dependency"):
                    print(f" -> [ ", end="")
                    deps = m.dependency
                    point = deps[0].point
                    if point.milestoneID == 'END':
                        print(f"{point.planItemID}", end="")
                    else:
                        print(f"{point.planItemID} ({point.milestoneID})", end="")
                    for d in deps[1:]:
                        pn = d.point
                        if pn.milestoneID == 'END':
                            print(f", {pn.planItemID}", end="")
                        else:
                            print(
                                f", {pn.planItemID} ({pn.milestoneID})", end="")
                    print(" ]", end="")
                print()
                count_m += 1
            print()
        count += 1
    print(f"Plan status: {colored_status(plan.status)}")


def main():
    args = docopt(__doc__, version=f"fom_plan.py version {__version__}")
    orderid = args["<orderid>"]
    orderid_path = Path(orderid)
    wsdl = Path(args["--wsdl"]).expanduser() if args["--wsdl"] else None
    envs_dir = Path(_envs_dir).expanduser().resolve()
    location = None
    sort_items = args["-s"]
    show_milestones = args["-m"]
    if args["-e"]:
        dev = (envs_dir / args["-e"]).with_suffix(".conf")
        with open(dev) as f:
            location = f.read()
    else:
        dev = (envs_dir / orderid_path.parent.name).with_suffix(".conf")
        with open(dev) as f:
            location = f.read()
    if args["-p"]:
        orderid = Path(orderid_path).resolve().name
        if wsdl is None:
            wsdl = Path(_default_wsdl_path).expanduser()
    if wsdl is None:
        _error("Please specify WSDL with '--wsdl' option")
        sys.exit(1)
    #client = Client(wsdl.resolve().as_uri(), plugins=[ExternalBusinessPlugin()])
    client = Client(wsdl.resolve().as_uri())
    response = process_request(client, orderid, location=location)
    pretty(orderid, response, sort_items=sort_items,
           show_milestones=show_milestones)


if __name__ == "__main__":
    main()
