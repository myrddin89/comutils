#! /usr/bin/env python

"""dbquery.py -- Query COM DB

USAGE:
    dbquery.py [-] [-hVCfsx] [-c CONFIG -l LIMIT -Q QUERY -d DB] [<orderid> <spec>]

If no options ('-f', '-s') are provided, reads an SQL
statement to run from stdin, otherwise see options description for more information.

If '-' is present, it reads query from stdin.

The configuration file for DB is assumed to be found in the parent directory
of <orderid>, with name "db_config.json", unless one specifies it explicitly
with '-c'.

ARGUMENTS:
    <orderid>               Order Id to use.
    <spec>                  Specify which query to run.

OPTIONS:
    -h, --help              Print this help message and exit.
    -V, --version           Print version information and exit.
    -C                      Print result as CSV to stdout.
    -d DB                   Database to use: one of 'bw', 'fom'.
    -c CONFIG               Use CONFIG for connection parameters.
                            It should be a JSON document defining
                            the following:
                            {
                                "username": "...",
                                "password": "...",
                                "connectionURL": "..."
                            }.
    -f                      Read query from QUERY. It should be a
                            JSON file with entries of the form:
                            {
                                "query": "select * from 'table' ..."
                            }. The <spec> argument is then used to
                            select the query to run.
    -s                      Read SQL statement to execute from QUERY.
    -l LIMIT                Limit to LIMIT the number of rows displayed.
    -Q QUERY                Query file.
    -x                      Creates order directory if it does not exist."""


import sys
from os import PathLike
from typing import Dict, Union
from docopt import docopt
import yaml
from pathlib import Path
from comutils.serverconfig import ServerConfig
from comutils import dbconnection as db
from comutils import sshforwarder as ssh

__version__ = "v0.1.0"
_default_config_path = Path("~/com/config")
_default_config_name = "db_config.json"
_default_queries = Path("~/com/db_queries.yml")
_default_db = "bw"


def pinfo(msg):
    print(msg, file=sys.stderr)


def perror(e):
    print(e, file="sys.stderr")


def get_config(args: Dict) -> ServerConfig:
    if args["-c"]:
        config_path = Path(args["-c"])
    else:
        env = Path(args["<orderid>"]).parent.name
        config_path = _default_config_path / env / _default_config_name
    db_kind = args["-d"] if args["-d"] else _default_db
    return db.serverconfig(config_path.expanduser().resolve().as_posix(), db_kind)


def query_json(path, query, **kwargs):
    if path is None:
        path = _default_queries.expanduser().resolve()
    path = Path(path).resolve()
    with open(path) as f:
        query_document = yaml.safe_load(f)
    sql_query = query_document[query] if query else query_document["status"]
    return db.build_query(sql_query, **kwargs)


def query_sql(path: PathLike):
    if path is None:
        return
    with open(path) as f:
        sql = f.read()
    return sql


def get_query(args: Dict):
    query = args["-Q"]
    spec = args["<spec>"]
    order_id = args["<orderid>"]
    if args["-f"]:
        if order_id is None:
            raise RuntimeError("Please specify order_id")
        order_path = Path(order_id)
        if args["-x"]:
            order_path.mkdir(exist_ok=True, parents=True)
        order_id = order_path.stem
        query = query_json(query, spec, order_id=order_id)
    elif args["-s"]:
        query = query_sql(query)
    elif args["-"]:
        query = sys.stdin.read()
    else:
        query = spec
    return query


def db_query(config: ServerConfig, query: str, limit: Union[int, None] = None):
    with db.connect(config) as c:
        try:
            df = db.read_sql(query, c, limit=limit)
        except Exception as e:
            perror(e)
            sys.exit(1)
    return df


def main():
    args = docopt(__doc__, version=f"dbquery.py version {__version__}")
    to_csv = args["-C"]
    limit = int(args["-l"]) if args["-l"] else None
    db_config = get_config(args)
    try:
        query = get_query(args)
    except Exception as e:
        perror(e)
        sys.exit(1)
    if query is None:
        print("Nothing to do, exiting")
        sys.exit()
    df = ssh.with_tunnel(db_query, db_config, query, limit)
    if to_csv:
        print(df.to_csv())
    else:
        print(df)


if __name__ == '__main__':
    main()
