#! /usr/bin/env python

"""teapp.py -- Query BW applications status

USAGE:
    teapp.py [-] [-hV] <env>

ARGUMENTS:
    <env>                   Get info for environment '<env>'.

OPTIONS:
    -h, --help              Print this help message and exit.
    -V, --version           Print version information and exit."""

import sys
from pathlib import Path
from docopt import docopt
from comutils.comutils import tea
from comutils.comutils.serverconfig import ServerConfig
from comutils.comutils import sshforwarder as ssh

__version__ = "v0.1.0"

_default_config_dir = Path("./config/")
_default_config_name = "tea.json"


def main():
    args = docopt(__doc__, version=f"teapp.py version {__version__}")
    env = args["<env>"]
    conf_path = _default_config_dir / env / _default_config_name
    config = ServerConfig(conf_path)
    with ssh.create_tunnel(config):
        server = tea.connect_tea(config)
        bw = server.refresh_().products["BusinessWorks"]
        try:
            print(bw.members.refresh_()['Fastweb-COM'].members)
        except Exception as e:
            print(e)
            sys.exit(1)


if __name__ == '__main__':
    main()
