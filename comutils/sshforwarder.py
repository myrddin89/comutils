from typing import Callable
from sshtunnel import open_tunnel
from .serverconfig import ServerConfig


def create_tunnel(config: ServerConfig):
    return open_tunnel(
        (config.gateway, 22),
        ssh_username=config.ssh_username,
        ssh_password=config.ssh_password,
        remote_bind_address=(config.remote_hostname, config.remote_port),
        local_bind_address=(config.local_hostname, config.local_port)
    )


def with_tunnel(f: Callable, config: ServerConfig, *args, **kwargs):
    with create_tunnel(config):
        result = f(config, *args, **kwargs)
    return result
