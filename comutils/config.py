from typing import Dict
import sys
import toml
from pathlib import Path
import logging

_default_config = Path.home() / "com" / "config" / "gitea_config.toml"
_log = logging.getLogger(__name__)


class Config(object):
    def __init__(self, config_dict: Dict) -> None:
        for k, v in config_dict.items():
            setattr(self, k, v)

    def __str__(self):
        to_repr_dict = self.__dict__
        del to_repr_dict["auth"]
        return f"Config({to_repr_dict})"


def gitea_config(args: dict):
    config_arg = args.get("-f")
    config_path = Path(config_arg) if config_arg else _default_config
    try:
        config = toml.load(config_path.resolve())
    except Exception as e:
        _log.error(f"Cannot read configuration '{config_path}': {e}")
        sys.exit(1)
    return config


def auth_info(config):
    if 'authentication' not in config:
        raise RuntimeError("Authentication section not found in configuration")
    auth = config["authentication"]
    token = auth["token"]
    user = auth["user"]
    return user, token


def repo_info(config):
    if 'repository' not in config:
        raise RuntimeError("Repository section not found in configuration")
    repo = config["repository"]
    return repo
