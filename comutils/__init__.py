import logging

logging.basicConfig(level=logging.INFO,
                    style='{',
                    format="{levelname:5s}|{asctime:s}|{name:s}(l.{lineno:d}) ==> {message:s}")
