import requests as api


def _check_args(**kwargs):
    for k in kwargs.keys():
        if not kwargs.get(k):
            raise RuntimeError(f"Please provide {k}")

def api_user(name, **kwargs):
    _check_args(name=name)
    return (f"users/{name}", api.get, None, None)



def api_branches(name, owner, **kwargs):
    _check_args(repository=name, owner=owner)
    return (f"repos/{owner}/{name}/branches", api.get, None, None)


def api_branch(name, owner, branch, **kwargs):
    _check_args(repository=name, owner=owner, branch=branch)
    return (f"repos/{owner}/{name}/branches/{branch}", api.get, None, None)


def api_create_branch(name, owner, new, old=None, **kwargs):
    _check_args(**{"repository": name, "owner": owner, "new branch": new})
    payload = {
        "new_branch_name": new,
        "old_branch_name": old
    }
    return (f"repos/{owner}/{name}/branches", api.post, payload, None)


def api_repos(name, **kwargs):
    _check_args(organization=name)
    return (f"repos/search", api.get, None, kwargs)


def api_pr(name, owner, **kwargs):
    _check_args(repository=name, owner=owner)
    return (f"repos/{owner}/{name}/pulls", api.get, None, kwargs)

def api_create_pr(name, owner, **kwargs):
    _check_args(repository=name, owner=owner)
    if not kwargs:
        raise RuntimeError("Please provide payload to create pull request")
    return (f"repos/{owner}/{name}/pulls", api.post, kwargs, None)


def api_commits(name, owner, **kwargs):
    _check_args(name=name, owner=owner)
    return (f"repos/{owner}/{name}/commits", api.get, None, kwargs)
