import sys
import logging
from . import gitea_api_def as defs
from ..config import Config


logger = logging.getLogger(__name__)


def bearer(token):
    return {"Authorization": f"Bearer {token}"}


def gitea_api(base_url, request):
    logger.debug(f"REQUEST: {request}")
    return f"{base_url}/{request}"


request_mapping = {
    "user": defs.api_user,
    "branches": defs.api_branches,
    "branch": defs.api_branch,
    "create_branch": defs.api_create_branch,
    "pr": defs.api_pr,
    "create_pr": defs.api_create_pr,
    "repos": defs.api_repos,
    "commits": defs.api_commits
}


def gitea_request(api_func, base_url, request, **kwargs):
    url = gitea_api(base_url, request)
    logger.debug(f"URL = {url}")
    return api_func(url, **kwargs)


def gtea(config: Config):
    if config.command is None:
        api_mapping = request_mapping
        print(list(api_mapping.keys()))
        sys.exit(0)
    api_mapping = request_mapping
    request, fapi, payload, params = api_mapping[config.command](
        name=config.name, owner=config.owner, **config.url_params)
    response = gitea_request(
        fapi, config.base_url, request, auth=config.auth, json=payload, params=params)
    return response
