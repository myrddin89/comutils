from os import PathLike
import json
from typing import Dict


class ServerConfig(object):
    def __init__(self, db_config: Dict) -> None:
        super().__init__()
        # with open(config_path) as f:
            # db_config = json.load(f)
        for k, v in db_config.items():
            setattr(self, k, v)

def serverconfig(config_path: PathLike, db_kind: str) -> ServerConfig:
    with open(config_path) as f:
        db_config = json.load(f)
    return ServerConfig(db_config[db_kind])
